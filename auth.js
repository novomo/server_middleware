/*
    Auth functions
*/
const jwt = require("jsonwebtoken");
const mysqlx = require("@mysql/xdevapi");
const {
  MYSQL_USER,
  MYSQL_PASS,
  MYSQL_HOST,
  JWT_KEY,
  RJWT_KEY,
  MYSQL_DATABASE,
} = require("../env.js");

// Checks token in header
let { client, reconnect } = require("../data/mysql/sql_connection");

const IP = require("ip");
const upload_error = require("../node_error_functions/upload_error");

module.exports = async (contextToken) => {
  try {
    console.log(contextToken);
    // Gets the sent token
    // if no token returns null incase of public paths
    if (!contextToken || contextToken === "") {
      return [null, null, null];
    }
    // for filtering out unauthorised access from web app
    if (
      contextToken.includes("no token") ||
      contextToken.includes("not verified") ||
      contextToken.includes("no user")
    ) {
      return [null, null, null];
    }
    // obtains token value from auth token
    const token = contextToken.split(" ")[1];
    // If no token user can only use none protected  routes
    if (!token || token === "") {
      return [null, null, null];
    }
    // If it is a cookie
    if (contextToken.includes("Bearer")) {
      let decodedToken;
      // decodes the token to get information
      try {
        decodedToken = jwt.verify(token, JWT_KEY);
        console.log(decodedToken);
        // if invalid token return null.
        if (!decodedToken) {
          return [null, null, null];
        }

        // returns the decoded information
        return [token, decodedToken.userId, decodedToken.level];
      } catch (err) {
        //console.log("token error");
        console.log(err);
        return [null, null, null];
      }
    } else if (contextToken.includes("Extension")) {
      let decodedToken;
      // decodes the token to get information
      ////console.log(token)
      try {
        decodedToken = jwt.verify(token, RJWT_KEY);
        ////console.log(decodedToken)
      } catch (err) {
        //console.log(err)
        return [null, null, null];
      }
      if (!decodedToken) {
        return [null, null, null];
      }
      // returns the decoded information
      return [token, decodedToken.userId, decodedToken.level];
    } else if (contextToken.includes("API")) {
      ////console.log(token);

      let session;
      try {
        session = await client.getSession();
      } catch (err) {
        client = await reconnect();
        session = await client.getSession();
      }
      const mysqlDb = await session.getSchema(MYSQL_DATABASE);
      let userTable = await mysqlDb.getTable("users");
      let user = await userTable
        .select()
        .where("accessToken = :accessToken")
        .bind("accessToken", token)
        .execute();
      user = user.fetchOne();

      ////console.log(user);
      if (!user) {
        return [null, null, null];
      }
      return [token, user[0], user[11]];
    } else {
      return [null, null, null];
    }
    await session.close();
  } catch (err) {
    console.log(err);
    upload_error({
      errorTitle: "Error in API Auth",
      machine: IP.address(),
      machineName: "API",
      errorFileName: __filename.slice(__dirname.length + 1),
      err: err,
      critical: true,
    });
  }
};
