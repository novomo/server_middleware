/* 
    Token Functions
*/
// node modules
const { sign } = require("jsonwebtoken");
const IP = require('ip');
const upload_error = require("../node_error_functions/upload_error")
const { JWT_KEY } = require("../env.js");
module.exports.sendRefreshToken = (res, token) => {
  res.cookie("jid", token, {
    path: "/refresh_token",
    sameSite: "none",
    secure: true,
  });
  //console.log(token);
  //console.log("sent token");
};

module.exports.sendExtensionToken = (res, extensionOn) => {
  try{
  res.cookie("eid", extensionOn, {
    path: "/extension_token",
    sameSite: "none",
    secure: true,
  });
} catch (err) {
  console.log(err)
  upload_error({
    errorTitle: "Error Sending Extension Token",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};
module.exports.createAccessToken = (user) => {
  try{
  return sign({ userId: user.userID, level: user.level }, JWT_KEY, {
    expiresIn: "7d",
  });
} catch (err) {
  console.log(err)
  upload_error({
    errorTitle: "Error Sending Login Token",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};

module.exports.createRefreshToken = (user) => {
  ////console.log(process.env.RJWT_KEY)
  try{
  return sign(
    { userId: user.userID, level: user.level, tokenVersion: 1 },
    JWT_KEY,
    {
      expiresIn: "7d",
    }
  );
} catch (err) {
  console.log(err)
  upload_error({
    errorTitle: "Error Sending Refresh Token",
    machine: IP.address(),
    machineName: "API",
    errorFileName: __filename.slice(__dirname.length + 1),
    err: err,
    critical: true
  })
}
};
